<div class="post">
    <div class="d-flex mb-2 justify-content-between">
        <div class="d-flex justify-items-center font-weight-bold">
            Replied to &nbsp;<a href="/show">How do I keep the value of a checkbox in an invalid form?</a>
        </div>
        <span class="text-success"><i class="fa fa-check"></i>
            Solution
        </span>
    </div>

    <p class="card-text">
        I am working on a Laravel 8 blogging application. The "Add article" form has a "switch" (checkbox) that lets the user choose whether or not the post will be a featured one.
    </p>
    <div class="d-flex justify-content-between">
        <div class="">
            <a href="#" class="card-link text-secondary"><i class="far fa-thumbs-up text-success"></i> <span class="ml-1">1</span></a>
        </div>
        <div class="d-flex align-items-center">

            <div class="dropdown ml-4">
                <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Edit</a>
                    <a class="dropdown-item" href="#">Hapus</a>
                </div>
            </div>
        </div>
    </div>
</div>
