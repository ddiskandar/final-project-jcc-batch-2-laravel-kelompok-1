@extends('layouts.master')

@section('content-header')
<div class="d-flex justify-content-between">
    <h1 class="font-weight-bold">Notifications</h1>
    <a href="#" class="btn btn-danger">Delete All Notifications</a>
</div>
@endsection

@section('main-content')
<div class="row">
    <div class="col">

        <div class="card ">
            <div class="card-header ">
                <div class="d-md-flex justify-content-between">
                    <div class="d-md-flex justify-items-center">
                        <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                        <a href="/user/profile" class="ml-2 font-weight-bold">Author</a> &nbsp; commented on &nbsp;
                        <div><a href="/show" class="font-weight-bold ">How do I keep the value of a checkbox in an invalid form?</a></div>
                    </div>
                    <div class="text-muted">1 hari yang lalu</div>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
            </div>
        </div>
        <div class="card ">
            <div class="card-header ">
                <div class="d-md-flex justify-content-between">
                    <div class="d-md-flex justify-items-center">
                        <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                        <a href="/user/profile" class="ml-2 font-weight-bold">Author</a> &nbsp; commented on &nbsp;
                        <div><a href="/show" class="font-weight-bold ">How do I keep the value of a checkbox in an invalid form?</a></div>
                    </div>
                    <div class="text-muted">1 hari yang lalu</div>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
            </div>
        </div>
        <div class="card ">
            <div class="card-header ">
                <div class="d-md-flex justify-content-between">
                    <div class="d-md-flex justify-items-center">
                        <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                        <a href="/user/profile" class="ml-2 font-weight-bold">Author</a> &nbsp; commented on &nbsp;
                        <div><a href="/show" class="font-weight-bold ">How do I keep the value of a checkbox in an invalid form?</a></div>
                    </div>
                    <div class="text-muted">1 hari yang lalu</div>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
            </div>
        </div>

    </div>
</div>
@endsection
