
<div class="post">
    <div class="row">
        <div class="col-auto d-none d-sm-block">
            <img data-src="holder.js/200x200" class="" alt="200x200" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17fdd49dff5%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17fdd49dff5%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.421875%22%20y%3D%22104.5%22%3E170x170%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 170px; height: 170px;">
        </div>
        <div class="col">
            <div class="d-flex mb-2 justify-content-between">
                <span class="text-muted">1 hari yang lalu</span>
                <div>
                    <a href="#" class="btn px-2 btn-xs btn-outline-warning">Laravel</a>
                </div>
            </div>

            <a href="/show" >
                <h3 class="font-weight-bold">How do I keep the value of a checkbox in an invalid form?</h3>
            </a>
            <p class="card-text">
                I am working on a Laravel 8 blogging application. The "Add article" form has a "switch" (checkbox) that lets the user choose whether or not the post will be a featured one.
            </p>
            <div class="d-flex justify-content-between">
                <div class="">
                    <a href="#" class="card-link text-secondary"><i class="far fa-thumbs-up text-success"></i> <span class="ml-1">1</span></a>
                    <span class="card-link text-secondary"><i class="far fa-comments"></i> <span class="ml-1">5</span></span>
                    <span class="card-link text-secondary"><i class="far fa-eye"></i> <span class="ml-1">5</span></span>
                    <span class="text-success ml-4"><i class="fa fa-check"></i>
                        Solved
                    </span>
                </div>
                <div class="dropdown ml-4">
                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
