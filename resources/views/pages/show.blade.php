@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold my-3">How do I keep the value of a checkbox in an invalid form?</h1>
<a href="#" class="card-link btn px-2 btn-xs btn-outline-warning">Laravel</a>
<a href="#" class="card-link text-secondary"><i class="fa fa-comment"></i> <span class="ml-1">5</span></a>
<a href="#" class="card-link text-secondary"><i class="fa fa-eye"></i> <span class="ml-1">5</span></a>
@endsection

@section('main-content')
<div class="card">
    <div class="card-header">
        <div class="d-flex justify-items-center">
            <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
            <a href="/user/profile" class="ml-2 font-weight-bold">Author</a>
            <span class="ml-2 text-muted">1 hari yang lalu</span>
        </div>
    </div>
    <div class="card-body">
        <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
        <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
        <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam, consectetur dolores numquam reprehenderit architecto, voluptates laborum explicabo rerum at esse soluta! Temporibus excepturi esse quaerat molestias quos alias iusto amet?</p>
        <div class="d-flex justify-content-between">
            <div class="">
                <a href="#" class="card-link text-secondary">
                    <i class="far fa-thumbs-up text-success"></i>
                    <span class="ml-1 mr-2">1</span>
                    <i class="text-muted"> Kamu menyukai diskusi ini</i>
                </a>
            </div>
            <div class="d-flex align-items-center">
                <div class="dropdown ml-4">
                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Hapus</a>
                        <a class="dropdown-item" href="#">Tandai Spam</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <!-- The time line -->
    <div class="timeline">
        <!-- timeline time label -->
        <div class="time-label">
            <span class="bg-red px-2">Replies</span>
        </div>
        <!-- /.timeline-label -->

        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-items-center">
                    <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                    <a href="/user/profile" class="ml-2 font-weight-bold">Author</a>
                    <span class="ml-2 text-muted">1 hari yang lalu</span>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
                <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam, consectetur dolores numquam reprehenderit architecto, voluptates laborum explicabo rerum at esse soluta! Temporibus excepturi esse quaerat molestias quos alias iusto amet?</p>
                <div class="d-flex justify-content-between">
                    <div class="">
                        <a href="#" class="card-link text-secondary">
                            <i class="far fa-thumbs-up "></i>
                            <span class="ml-1 mr-2">1</span>
                        </a>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="dropdown ml-4">
                            <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Hapus</a>
                                <a class="dropdown-item" href="#">Tandai Spam</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-success card-outline">
            <div class="card-header ">
                <div class="d-flex justify-content-between">
                    <div class="d-flex justify-items-center">
                        <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                        <a href="/user/profile" class="ml-2 font-weight-bold">Author</a>
                        <span class="ml-2 text-muted">1 hari yang lalu</span>
                    </div>
                    <span class="text-success"><i class="fa fa-check"></i>
                        Solution
                    </span>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
                <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam, consectetur dolores numquam reprehenderit architecto, voluptates laborum explicabo rerum at esse soluta! Temporibus excepturi esse quaerat molestias quos alias iusto amet?</p>
                <div class="d-flex justify-content-between">
                    <div class="">
                        <a href="#" class="card-link text-secondary">
                            <i class="far fa-thumbs-up text-success"></i>
                            <span class="ml-1 mr-2">1</span>
                            <i class="text-muted"> Kamu menyukai diskusi ini</i>
                        </a>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="dropdown ml-4">
                            <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Hapus</a>
                                <a class="dropdown-item" href="#">Tandai Spam</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-items-center">
                    <img src="dist/img/user8-128x128.jpg" width="25px" class="img-circle" alt="User Image">
                    <a href="/user/profile" class="ml-2 font-weight-bold">Author</a>
                    <span class="ml-2 text-muted">1 hari yang lalu</span>
                </div>
            </div>
            <div class="card-body">
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas aliquid corrupti expedita rem nesciunt. Beatae, accusantium vero a similique aspernatur ex nihil nisi maxime voluptatum consequatur atque, voluptate nostrum expedita.</p>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, voluptate ad? Minima minus accusamus sapiente perferendis animi eos laborum in repellat iure quasi quod commodi, facere deserunt tempora! Ipsum, quis?</p>
                <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam, consectetur dolores numquam reprehenderit architecto, voluptates laborum explicabo rerum at esse soluta! Temporibus excepturi esse quaerat molestias quos alias iusto amet?</p>
                <div class="d-flex justify-content-between">
                    <div class="">
                        <a href="#" class="card-link text-secondary">
                            <i class="far fa-thumbs-up"></i>
                            <span class="ml-1 mr-2">1</span>
                        </a>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="dropdown ml-4">
                            <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Edit</a>
                                <a class="dropdown-item" href="#">Hapus</a>
                                <a class="dropdown-item" href="#">Tandai Spam</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <!-- /.col -->
</div>

<div class="row my-2">
    <h4 class="col">Write a Reply</h4>
</div>
<div class="row ">
    <div class="col mb-3">
        <form>
            <div class="form-group">
                <textarea id="summernote">
                    Place <em>some</em> <u>text</u> <strong>here</strong>
                </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection

@push('styles')
<!-- summernote -->
<link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

<style>
    .timeline {
        margin: 0;
    }
    .timeline>div>.timeline-item {
        margin: 0;
    }

    .timeline>div {
        margin-right: 0;
    }

    .note-toolbar {
        background: white;
    }
</style>
@endpush

@push('scripts')
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $(function () {
      // Summernote
      $('#summernote').summernote();
    })
  </script>
@endpush
