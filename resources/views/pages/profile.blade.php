@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold">Profile</h1>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-3">

    <!-- Profile Image -->
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
        <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                src="{{ asset('dist/img/user4-128x128.jpg') }}"
                alt="User profile picture">
        </div>

        <h3 class="profile-username text-center">John Example Doe</h3>

        <p class="text-muted text-center">username</p>

        <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
                <b>Threads</b> <a class="float-right">1,322</a>
            </li>
            <li class="list-group-item">
                <b>Replies</b> <a class="float-right">543</a>
            </li>
            <li class="list-group-item">
                <b>Solution</b> <a class="float-right">543</a>
            </li>
            <li class="list-group-item">
                <b>Likes</b> <a class="float-right">13,287</a>
            </li>
        </ul>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <!-- About Me Box -->
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">About Me</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <strong>Bio</strong>
            <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
            </p>
            <hr>
            <strong> Email</strong>
            <p class="text-muted">john@example.com</p>
            <hr>
            <strong>Umur</strong>
            <p class="text-muted">35 tahun</p>
            <hr>
            <strong>Alamat</strong>
            <p class="text-muted">Bandung</p>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
    <div class="card">
        <div class="card-header p-2">
        <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link" href="#activity" data-toggle="tab">Threads posted</a></li>
            <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Replies posted</a></li>
            <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Settings</a></li>
        </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane" id="activity">
                @include('pages.profile-threads')
                @include('pages.profile-threads')
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="timeline">
                @include('pages.profile-replies')
                @include('pages.profile-replies')
                @include('pages.profile-replies')
            </div>
            <!-- /.tab-pane -->

            <div class="active tab-pane" id="settings">
            <form class="form-horizontal">
                <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUserame" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputUserame" placeholder="Username">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUmur" class="col-sm-2 col-form-label">Umur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputUmur" placeholder="Umur">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="inputAlamat" placeholder="Alamat"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputBio" class="col-sm-2 col-form-label">Bio</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="inputBio" placeholder="Bio"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Submit</button>
                </div>
                </div>
            </form>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        </div><!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
