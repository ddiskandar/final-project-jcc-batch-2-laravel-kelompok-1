@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold">Leaderboard</h1>
@endsection

@section('main-content')
<div class="row">
    <div class="col">
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">1</span>
                        <img src="{{ asset('dist/img/user8-128x128.jpg') }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/user/profile" class="ml-3 font-weight-bold ">taylor</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">254 Threads</span>
                        <span class="card-link">64 Replies</span>
                        <span class="card-link">34 Solution</span>
                        <span class="card-link">45 Likes</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">2</span>
                        <img src="{{ asset('dist/img/user1-128x128.jpg') }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/user/profile" class="ml-3 font-weight-bold ">johndoe</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">164 Threads</span>
                        <span class="card-link">365 Replies</span>
                        <span class="card-link">63 Solution</span>
                        <span class="card-link">200 Likes</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">3</span>
                        <img src="{{ asset('dist/img/user4-128x128.jpg') }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/user/profile" class="ml-3 font-weight-bold ">ericlbarnes</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">87 Threads</span>
                        <span class="card-link">35 Replies</span>
                        <span class="card-link">16 Solution</span>
                        <span class="card-link">7 Likes</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">4</span>
                        <img src="{{ asset('dist/img/user6-128x128.jpg') }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/user/profile" class="ml-3 font-weight-bold ">Povilas</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">74 Threads</span>
                        <span class="card-link">23 Replies</span>
                        <span class="card-link">13 Solution</span>
                        <span class="card-link">46 Likes</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">5</span>
                        <img src="{{ asset('dist/img/user7-128x128.jpg') }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/user/profile" class="ml-3 font-weight-bold ">andrea</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">36 Threads</span>
                        <span class="card-link">25 Replies</span>
                        <span class="card-link">13 Solution</span>
                        <span class="card-link">7 Likes</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
