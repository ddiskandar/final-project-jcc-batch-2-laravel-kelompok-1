@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold my-3">Create a new thread</h1>
@endsection

@section('main-content')

<div class="row ">
    <div class="col mb-3">
        <form>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control is-invalid" id="subject" placeholder="Enter Subject">
                <div class="invalid-feedback">
                    Subject harus di isi
                </div>
            </div>
            <div class="form-group">
                <label>Category</label>
                <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="image">Image</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image">
                        <label class="custom-file-label" for="image">Choose file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Body</label>
                <textarea id="summernote" name="body">
                    Place <em>some</em> <u>text</u> <strong>here</strong>
                </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection

@push('styles')
<!-- summernote -->
<link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

<style>
    .note-toolbar {
        background: white;
    }
</style>
@endpush

@push('scripts')
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script>
    $(function () {
      // Summernote
      $('#summernote').summernote();

      bsCustomFileInput.init();
    })
  </script>
@endpush

