@extends('layouts.master')

@section('content-header')
<div class="d-flex mb-3 justify-content-between">
    <h1 class="font-weight-bold">Forum</h1>
    <a href="/create" class="btn btn-primary">New Discussion</a>
</div>

<div class="row">
    <div class="col-sm-6">
        <h5>12,567 diskusi</h5>
    </div><!-- /.col -->
    <div class="col-sm-6 ">
        <div class="form-row">
            <div class="col">
                <select class="form-control">
                    <option>Latest</option>
                    <option>Top Likes</option>
                    <option>My Threads</option>
                    <option>Spam Thread</option>
                    <option>Spam Comment</option>
                </select>
            </div>
            <div class="col">
                <select class="form-control">
                    <option>Semua Category</option>
                    <option>Laravel</option>
                    <option>VueJS</option>
                    <option>Livewire</option>
                    <option>AlpineJs</option>
                </select>
            </div>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('main-content')
<div class="row">
    <div class="col">
        @include('pages.card')
        @include('pages.card')
        @include('pages.card')
    </div>
</div>

@endsection
