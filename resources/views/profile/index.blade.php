@extends('layouts.master')

@section('content-header')
    <h1 class="font-weight-bold">Profile</h1>
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class=" img-fluid img-circle" src="{{ $users->profile_photo_url }}"
                            alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{ $user->name }}</h3>
                    <p class="text-muted text-center">{{ $users->username }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Threads</b> <a class="float-right">{{ $users->threads->count() }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Replies</b> <a class="float-right">{{ $users->replies->count() }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Likes</b> <a
                                class="float-right">{{ DB::table('likables')->where('user_id', $users->id)->count() }}</a>
                        </li>
                    </ul>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">About Me</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <strong>Bio</strong>
                    <p class="text-muted">
                        {{ $user->bio }}
                    </p>
                    <hr>
                    <strong> Email</strong>
                    <p class="text-muted">{{ $users->email }}</p>
                    <hr>
                    <strong>Umur</strong>
                    <p class="text-muted">{{ $user->age }}</p>
                    <hr>
                    <strong>Alamat</strong>
                    <p class="text-muted">{{ $user->address }}</p>
                    <strong>Website URL</strong>
                    <p class="text-muted">{{ $user->website_url }}</p>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Threads
                                posted</a></li>
                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Replies
                                posted</a></li>
                        @if (Auth::check() && auth()->id() == $user->user_id)
                            <li class="nav-item"><a class="nav-link " href="#settings" data-toggle="tab">Edit
                                    Your Profile</a></li>
                        @endif

                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            @forelse ($threads as $thread)
                                <div class="post">
                                    <div class="row">
                                        <div class="col-auto d-none d-sm-block">
                                            <img data-src="holder.js/200x200" class="" alt="200x200"
                                                src="{{ asset($thread->thumbnail_url) }}" data-holder-rendered="true"
                                                style="width: 170px; height: 170px;">
                                        </div>
                                        <div class="col">
                                            <div class="d-flex mb-2 justify-content-between">
                                                <span
                                                    class="text-muted">{{ $thread->created_at->diffForHumans() }}</span>
                                                <div>
                                                    <a href="/?category={{ $thread->category->slug }}"
                                                        class="btn px-2 btn-xs btn-outline-warning">{{ $thread->category->slug }}</a>
                                                </div>
                                            </div>

                                            <a href="{{ route('thread.show', $thread->slug) }}">
                                                <h3 class="font-weight-bold">{{ $thread->subject }}</h3>
                                            </a>
                                            <p class="card-text">
                                                {{ $thread->excerpt() }}
                                            </p>
                                            <div class="d-flex justify-content-between">
                                                <div class="">
                                                    <form method="POST" action="/thread/{{ $thread->slug }}/likes">
                                                        @csrf

                                                        <button type="submit"
                                                            class="display-button mr-3 @if (Auth::check() && $thread->isLikedBy(auth()->user())) text-success @else text-secondary @endif ">
                                                            <i class="far fa-thumbs-up "></i><span
                                                                class="ml-1 ">{{ $thread->likes->count() }}</span>
                                                        </button>
                                                        <span class="card-link text-secondary"><i
                                                                class="far fa-comments"></i><span
                                                                class="ml-1">{{ $thread->replies->count() }}</span></span>
                                                        <span class="card-link text-secondary"><i
                                                                class="far fa-eye"></i><span
                                                                class="ml-1">{{ $thread->views_count }}</span></span>
                                                    </form>
                                                </div>
                                                <div class="dropdown ml-4">
                                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        Action
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                        aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item"
                                                            href="{{ route('thread.edit', $thread->slug) }}">Edit</a>
                                                        <form
                                                            action="/threads/{{ $thread->id }}/{{ $thread->user_id }}"
                                                            method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <input type="submit" class="dropdown-item" value="Delete"
                                                                onclick="return confirm('Are you sure you want to delete?');">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="text-center my-5 h5 font-weight-bold">Kamu belum pernah memposting Thread, klik
                                    link dibawah ini untuk memposting thread! <br>
                                    <a href="{{ route('thread.create') }}" class="text-info">Posting Thread</a>
                                </p>
                            @endforelse
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="timeline">
                            {{-- Replies --}}
                            @forelse ($replies as $reply)
                                <div class="post">
                                    <div class="d-flex mb-2 justify-content-between">
                                        <div class="d-flex justify-items-center font-weight-bold">
                                            Replied to &nbsp;<a
                                                href="{{ route('thread.show', $reply->thread->slug) }}">{{ $reply->thread->subject }}</a>
                                        </div>
                                        @if ($reply->isThreadSolution())
                                            <span class="text-success"><i class="fa fa-check"></i>
                                                Solution
                                            </span>
                                        @endif
                                    </div>

                                    <p class="card-text">
                                        {!! $reply->body !!}
                                    </p>
                                    <div class="d-flex justify-content-between">
                                        <div class="">
                                            <form method="POST" action="/thread/{{ $reply->thread->slug }}/{{ $reply->id }}/likes">
                                            @csrf
            
                                                <button type="submit"
                                                    class="display-button @if (Auth::check() && $reply->isLikedBy(auth()->user())) text-success @else text-secondary @endif ">
                                                    <i class="far fa-thumbs-up "></i><span
                                                        class="ml-1 ">{{ $reply->likes->count() }}</span>
                                                </button>
                                                @if (Auth::check() && $reply->isLikedBy(auth()->user()))
                                                    <i class="text-success"> Kamu menyukai jawaban ini</i>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="d-flex align-items-center">

                                            <div class="dropdown ml-4">
                                                <button class="btn btn-sm btn-secondary dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    Action
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right"
                                                    aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item"
                                                        href="/replies/{{ $reply->id }}/edit">Edit</a>
                                                    <form action="/replies/{{ $reply->id }}/{{ $reply->user_id }}"
                                                        method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <input type="submit" class="dropdown-item" value="Delete"
                                                            onclick="return confirm('Are you sure you want to delete?');">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @empty
                                <p class="text-center my-5 h4 font-weight-bold">Kamu belum pernah mempsoting Reply.</p>
                            @endforelse
                        </div>
                        <!-- /.tab-pane -->
                        @if (Auth::check() && auth()->id() == $user->user_id)
                            <div class="tab-pane " id="settings">
                                <form class="form-horizontal" action="/profile/{{ $user->user_id }}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName" name="name"
                                                placeholder="Masukkan nama" value="{{ $user->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputUserame" class="col-sm-2 col-form-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputUserame" name="username"
                                                value="{{ $users->username }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" name="email"
                                                value="{{ $users->email }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputUmur" class="col-sm-2 col-form-label">Umur</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputUmur" name="age"
                                                placeholder="Contoh 20" value="{{ $user->age }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputAlamat" placeholder="Alamat"
                                                name="address">{{ $user->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputBio" class="col-sm-2 col-form-label">Bio</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputBio" placeholder="Bio" name="bio"
                                                placeholder="Bio">{{ $user->bio }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputBio" class="col-sm-2 col-form-label">Website URL</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputUmur"
                                                placeholder="Website URL " name="website_url"
                                                value="{{ $user->website_url }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">Edit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        @endif
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@push('styles')
    <style>
        .display-button {
            border: 0;
            outline: 0;
            background: none;
        }

        .display-button:focus {
            outline: none !important;
        }

    </style>
@endpush

@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endpush
