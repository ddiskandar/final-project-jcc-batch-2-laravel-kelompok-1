@extends('layouts.master')

@section('content-header')
<div class="d-flex mb-3 justify-content-between">
    <h1 class="font-weight-bold">Forum</h1>
    <div>
        @auth
            @if (auth()->user()->isAdmin())
                <a href="{{ route('category.index') }}" class="btn btn-danger">Categories</a>
            @endif
        @endauth
        <a href="{{ route('thread.create') }}" class="btn btn-primary">New Discussion</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <h5>{{ $data['thread_count'] }} diskusi</h5>
    </div><!-- /.col -->
    <div class="col-sm-6 ">
        <div class="form-row">
            <div class="col">
                <select class="form-control" onchange="location = this.value">
                    <option value="/?{{ http_build_query(request()->except('page', 'popular', 'author')) }}">Latest</option>
                    <option @if (\Request::has('popular') && (\Request::query('popular') == 'views')) selected @endif value="/?{{ http_build_query(request()->except('page', 'spam', 'author', 'popular')) }}&popular=views">Top Views</option>
                    <option @if (\Request::has('popular') && (\Request::query('popular') == 'likes')) selected @endif value="/?{{ http_build_query(request()->except('page', 'spam', 'author', 'popular')) }}&popular=likes">Top Likes</option>
                    <option @if (\Request::has('popular') && (\Request::query('popular') == 'replies')) selected @endif value="/?{{ http_build_query(request()->except('popular', 'page', 'spam', 'author',)) }}&popular=replies">Top Replies</option>
                    @auth
                        <option @if (\Request::has('author')) selected @endif value="/?{{ http_build_query(request()->except('page', 'popular')) }}&author={{ Auth()->id() }}">My Threads</option>
                        @if (auth()->user()->isModerator())
                            <option @if (\Request::has('spam') && (\Request::query('spam') == 'threads'))  selected @endif value="/?{{ http_build_query(request()->except('page', 'popular', 'author')) }}&spam=threads">Spam Reports</option>
                        @endif
                    @endauth
                </select>
            </div>
            <div class="col">
                <select class="form-control" onchange="location = this.value">
                    <option value="?">Semua Category</option>
                    @foreach (\DB::table('categories')->get(['slug', 'name']) as $category)
                    <option @if (\Request::has('category') && (\Request::query('category') == $category->slug) ) selected @endif value="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('main-content')
<div class="row">
    <div class="col">
        @forelse ($threads as $thread)
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-auto d-none d-sm-block">
                        <img class="img-thumbnail" alt="200x200" src="{{ asset($thread->thumbnail_url) }}" style="width: 170px;  height: 170px;">
                    </div>
                    <div class="col">
                        <div class="d-flex mb-2 justify-content-between">
                            <div class="d-sm-flex justify-items-center">
                                <div class="d-flex justify-items-center">
                                    <img src="{{ $thread->creator->profile_photo_url }}" width="25px" height="25px" class="img-circle" alt="User Image">
                                    <a href="/profile/{{ $thread->creator->id }}" class="ml-2 font-weight-bold ">{{ $thread->creator->username }}</a>
                                </div>
                                <div class="sm-ml-2 text-muted">{{ $thread->created_at->diffForHumans() }}</div>
                            </div>
                            <div class="d-flex align-items-center">
                                <a href="/?category={{ $thread->category->slug }}" class="btn px-2 btn-xs btn-outline-warning">{{ $thread->category->name }}</a>
                            </div>
                        </div>

                        @auth
                            @if (auth()->user()->isModerator() && $thread->spam_reports > 0)
                                <div class="mb-1 text-danger">spam reported : {{ $thread->spam_reports }}</div>
                            @endif
                        @endauth
                        <h4 class="font-weight-bold">
                            <a href="{{ route('thread.show', $thread->slug) }}">
                                {{ $thread->subject }}
                            </a>
                        </h4>
                        <p class="card-text">
                            {{ $thread->excerpt() }}
                        </p>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-content-middle">
                                <div class="card-link text-secondary">
                                    <form method="POST"
                                        action="/thread/{{ $thread->slug }}/likes"
                                    >
                                        @csrf

                                        <button type="submit" class="display-button @if (Auth::check() && $thread->isLikedBy(auth()->user())) text-success @else text-secondary @endif ">
                                            <i class="far fa-thumbs-up "></i><span class="ml-1 ">{{ $thread->likes->count() }}</span>
                                        </button>
                                    </form>
                                </div>
                                <span class="card-link text-secondary"><i class="far fa-comments"></i> <span class="ml-1">{{ $thread->replies_count }}</span></span>
                                <span class="card-link text-secondary"><i class="far fa-eye"></i> <span class="ml-1">{{ $thread->views_count }}</span></span>
                                @if ($thread->isSolved())
                                <span class="text-success ml-4"><i class="fa fa-check"></i>
                                    Solved
                                </span>
                                @endif
                            </div>
                            <div >
                                @if (Auth::check() && (auth()->user()->isModerator() OR auth()->id() == $thread->user_id) )
                                    <div class="dropdown ml-4">
                                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            @if (auth()->id() == $thread->user_id)
                                                <a class="dropdown-item" href="{{ route('thread.edit', $thread->slug) }}">Edit</a>
                                            @endif
                                            @if (auth()->id() == $thread->user_id OR auth()->user()->isModerator())
                                            <form action="/thread/{{ $thread->slug }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input
                                                type="submit"
                                                    class="dropdown-item"
                                                    value="Delete"
                                                    onclick="return confirm('Are you sure you want to delete?');"
                                                    >
                                            </form>
                                            @endif
                                            @if (auth()->user()->isModerator())
                                            <form action="/thread/{{ $thread->slug }}/spam" method="POST">
                                                @csrf
                                                <input
                                                    type="submit"
                                                    class="dropdown-item"
                                                    value="Mark as Spam"
                                                    onclick="return confirm('Are you sure you want to report spam this thread?');"
                                                >
                                            </form>
                                            <form action="/thread/{{ $thread->slug }}/not-spam" method="POST">
                                                @csrf
                                                <input
                                                    type="submit"
                                                    class="dropdown-item"
                                                    value="Not Spam"
                                                    onclick="return confirm('Are you sure you want to mark this idea as NOT spam? This will reset the spam counter to 0.');"
                                                >
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
            <p class="text-center my-5 h4 font-weight-bold">No Thread yet. Please check back later.</p>
        @endforelse

        @isset ($threads)
        <div class="d-flex justify-content-center">
            {{ $threads->appends('category', \Request::query('category'))->links() }}
        </div>
        @endisset
    </div>
</div>

@endsection

@push('styles')
    <style>
        .display-button {
            border:0;
            outline:0;
            background: none;
        }
        .display-button:focus {
            outline:none!important;
        }
    </style>
@endpush

@push('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endpush
