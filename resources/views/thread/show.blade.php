@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold my-3">{{ $thread->subject }}</h1>
<a href="/?category={{ $thread->category->slug }}" class="card-link btn px-2 btn-xs btn-outline-warning">{{ $thread->category->name }}</a>
<span class="card-link text-secondary"><i class="fa fa-comment"></i> <span class="ml-1">{{ $thread->replies->count() }}</span></span>
<span class="card-link text-secondary"><i class="fa fa-eye"></i> <span class="ml-1">{{ $thread->views_count }}</span></span>
@endsection

@section('main-content')
<div class="card">
    <div class="card-header">
        <div class="d-flex justify-items-center">
            <img src="{{ $thread->creator->profile_photo_url }}" width="25px" class="img-circle" alt="User Image">
            <a href="/profile/{{ $thread->creator->id }}" class="ml-2 font-weight-bold">{{ $thread->creator->username }}</a>
            @if ($thread->creator->isModerator())
                <span class="card-link btn mx-2 px-2 btn-xs font-weight-bold btn-info">{{ $thread->creator->type == 2 ? 'MODERATOR' : 'ADMIN'}}</a></span>
            @endif
            <span class="ml-2 text-muted">{{ $thread->created_at->diffForHumans() }}</span>
        </div>
    </div>
    <div class="card-body">
        @auth
            @if (auth()->user()->isModerator() && $thread->spam_reports > 0)
                <div class="mb-1 text-danger">spam reported : {{ $thread->spam_reports }}</div>
            @endif
        @endauth
        <div class="d-none d-sm-block mb-4">
            <img class="img-fluid" alt="200x200" src="{{ asset($thread->thumbnail_url) }}" style="width: 370px;  ">
        </div>
        <div class="mb-2">
            {!! $thread->body !!}
        </div>
        <div class="d-flex justify-content-between">
            <div class="card-link text-secondary">
                <form method="POST"
                    action="/thread/{{ $thread->slug }}/likes"
                >
                    @csrf

                    <button type="submit" class="display-button @if (Auth::check() && $thread->isLikedBy(auth()->user())) text-success @else text-secondary @endif ">
                        <i class="far fa-thumbs-up "></i><span class="ml-1 ">{{ $thread->likes->count() }}</span>
                    </button>
                    @if (Auth::check() && $thread->isLikedBy(auth()->user()))
                    <i class="text-success"> Kamu menyukai diskusi ini</i>
                    @endif
                </form>
            </div>
            @auth
                @if (auth()->check() && (auth()->user()->isModerator() OR auth()->id() == $thread->user_id))
                <div class="d-flex align-items-center">
                    <div class="dropdown ml-4">
                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            @if (auth()->id() == $thread->user_id)
                            <a class="dropdown-item" href="{{ route('thread.edit', $thread->slug) }}">Edit</a>
                            @endif
                            @if (auth()->id() == $thread->user_id OR auth()->user()->isModerator())
                            <form action="/thread/{{ $thread->slug }}" method="post">
                                @csrf
                                @method('delete')
                                <input
                                    type="submit"
                                    class="dropdown-item"
                                    value="Delete"
                                    onclick="return confirm('Are you sure you want to delete?');"
                                >
                            </form>
                            @endif
                            @if (auth()->user()->isModerator())
                            <form action="/thread/{{ $thread->slug }}/spam" method="POST">
                                @csrf
                                <input
                                    type="submit"
                                    class="dropdown-item"
                                    value="Mark as Spam"
                                    onclick="return confirm('Are you sure you want to report spam this thread?');"
                                >
                            </form>
                            <form action="/thread/{{ $thread->slug }}/not-spam" method="POST">
                                @csrf
                                <input
                                    type="submit"
                                    class="dropdown-item"
                                    value="Not Spam"
                                    onclick="return confirm('Are you sure you want to mark this idea as NOT spam? This will reset the spam counter to 0.');"
                                >
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
                @endif
            @endauth
        </div>

    </div>
</div>

@if($thread->replies->count() > 0)
<div class="row">
    <div class="col-md-12">
    <!-- The time line -->
    <div class="timeline">
        <!-- timeline time label -->
        <div class="time-label">
            <span class="bg-red px-2">Replies</span>
        </div>
        <!-- /.timeline-label -->

        @forelse ($thread->replies as $reply)
            <div class="card {{ ($reply->isThreadSolution()) ? 'card-success card-outline' : '' }}">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex justify-items-center">
                            <img src="{{ $reply->owner->profile_photo_url }}" width="25px" class="img-circle" alt="User Image">
                            <a href="/profile/{{ $reply->owner->id }}" class="ml-2 font-weight-bold">{{ $reply->owner->username }}</a>
                            @if ($reply->owner->isModerator())
                                <span class="card-link btn mx-2 px-2 btn-xs font-weight-bold btn-info">{{ $reply->owner->type == 2 ? 'MODERATOR' : 'ADMIN'}}</a></span>
                            @endif
                            @if ($reply->user_id == $thread->user_id)
                                <span class=" mx-2 btn px-2 btn-xs font-weight-bold btn-outline-secondary">OP</a></span>
                            @endif
                            <span class="ml-2 text-muted">{{ $reply->created_at->diffForHumans() }}</span>
                        </div>
                        @if ($reply->isThreadSolution())
                        <span class="text-success"><i class="fa fa-check"></i>
                            Solution
                        </span>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @auth
                        @if (auth()->user()->isModerator() && $reply->spam_reports > 0)
                         <div class="mb-1 text-danger">spam reported : {{ $reply->spam_reports }}</div>
                        @endif
                    @endauth
                    <div class="mb-2">
                        {!! $reply->body !!}
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="card-link text-secondary">
                            <form method="POST"
                                action="/thread/{{ $thread->slug }}/{{ $reply->id }}/likes"
                            >
                                @csrf

                                <button type="submit" class="display-button @if (Auth::check() && $reply->isLikedBy(auth()->user())) text-success @else text-secondary @endif ">
                                    <i class="far fa-thumbs-up "></i><span class="ml-1 ">{{ $reply->likes->count() }}</span>
                                </button>
                                @if (Auth::check() && $reply->isLikedBy(auth()->user()))
                                <i class="text-success"> Kamu menyukai jawaban ini</i>
                                @endif
                            </form>
                        </div>
                        @auth
                            @if (auth()->check() && auth()->user()->isModerator() OR $reply->user_id == auth()->id())
                            <div class="d-flex align-items-center">
                                <div class="dropdown ml-4">
                                    <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @if (auth()->id() == $reply->user_id)
                                        <a class="dropdown-item" href="/replies/{{ $reply->id }}/edit">Edit</a>
                                        <form action="/thread/{{ $thread->slug }}/replies/{{ $reply->id }}/solution" method="post">
                                            @csrf
                                            <input
                                                type="submit"
                                                class="dropdown-item"
                                                value="Mark as Solution"
                                                onclick="return confirm('Are you sure you want to mark reply as solution?');"
                                            >
                                        </form>
                                        @endif

                                        @if (auth()->id() == $reply->user_id OR auth()->user()->isModerator())
                                        <form action="/thread/{{ $thread->slug }}/{{ $reply->id }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <input
                                                type="submit"
                                                class="dropdown-item"
                                                value="Delete"
                                                onclick="return confirm('Are you sure you want to delete?');"
                                            >
                                        </form>
                                        @endif

                                        @if (auth()->user()->isModerator())
                                        <form action="/thread/{{ $thread->slug }}/{{ $reply->id }}/spam" method="POST">
                                            @csrf
                                            <input
                                                type="submit"
                                                class="dropdown-item"
                                                value="Mark as Spam"
                                                onclick="return confirm('Are you sure you want to report spam this thread?');"
                                            >
                                        </form>
                                        <form action="/thread/{{ $thread->slug }}/{{ $reply->id }}/not-spam" method="POST">
                                            @csrf
                                            <input
                                                type="submit"
                                                class="dropdown-item"
                                                value="Not Spam"
                                                onclick="return confirm('Are you sure you want to mark this idea as NOT spam? This will reset the spam counter to 0.');"
                                            >
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
        @empty

        @endforelse

    </div>
    </div>
    <!-- /.col -->
</div>
@endif


@guest
    <div class="row pb-5">
        <div class="col">
            <p><a href="{{ route('login') }}">Sign in</a> to participate in this thread!</p>
        </div>
    </div>
@else
<div class="row my-2">
    <h4 class="col">Write a Reply</h4>
</div>
<div class="row ">
    <div class="col mb-3">
        <form method="POST" action="/thread/{{ $thread->slug }}/replies">
            @csrf

            <div class="form-group">
                <textarea
                    name="body"
                    id="summernote"
                    ></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endguest

@endsection

@push('styles')
<!-- summernote -->
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">

<style>
    .timeline {
        margin: 0;
    }
    .timeline>div>.timeline-item {
        margin: 0;
    }

    .timeline>div {
        margin-right: 0;
    }

    .note-toolbar {
        background: white;
    }

    .display-button {
        border:0;
        outline:0;
        background: none;
    }
    .display-button:focus {
        outline:none!important;
    }
</style>
@endpush

@push('scripts')
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function () {
      // Summernote
      $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview', 'help']],
        ]
      });
    })
  </script>
@endpush
