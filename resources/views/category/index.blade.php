@extends('layouts.master')

@section('content-header')
    <h1 class="font-weight-bold">Kategori</h1>
@endsection

@push('scripts')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#category").DataTable();
        });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('main-content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <a href="/category/create" class="btn btn-primary my-3"><i class="fas fa-plus"></i> Tambah Kategori</a>
                <table id="category" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($category as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->name }}</td>
                                <td width="65%">{{ $item->description }}</td>
                                <td>
                                    <form action="/category/{{ $item->id }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="/category/{{ $item->id }}" class="btn btn-info btn-sm"> Detail</a>
                                        <a href="/category/{{ $item->id }}/edit" class="btn btn-warning btn-sm"> Edit</a>
                                        <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h3>Data kosong!</h3>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
@endsection
