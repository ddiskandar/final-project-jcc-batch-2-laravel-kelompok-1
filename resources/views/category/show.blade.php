@extends('layouts.master')

@section('content-header')
    <h1 class="font-weight-bold">Kategori</h1>
@endsection

@section('main-content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Halaman Detail Kategori</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="active tab-pane" id="settings">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama Kategori</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="inputName"
                                value="{{ $category->name }}" disabled>
                        </div>
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="inputDescription" class="col-sm-2 col-form-label">Deskripsi Kategori</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" name="description" id="inputDescription" disabled>{{ $category->description }}</textarea>
                        </div>
                    </div>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                            <a href="/category" class="btn btn-danger">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
@endsection
