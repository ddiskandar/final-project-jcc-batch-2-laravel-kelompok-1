@extends('layouts.master')

@section('content-header')
<div class="d-flex justify-content-between">
    <h1 class="font-weight-bold">Notifications</h1>
    <div class="d-flex justify-items-center">
        <form action="/notifications" method="POST">
            @csrf
            <input type="submit" class="btn btn-success" value="Mark All as Read">
        </form>
        <form action="/notifications" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger ml-2" value="Delete All">
        </form>
    </div>
</div>
@endsection

@section('main-content')
<div class="row">
    <div class="col">

        @forelse ($notifications as $notification)
        <div class="card ">
            <div class="card-header ">
                <div class="d-md-flex justify-content-between">
                    <div class="d-md-flex justify-items-center">
                        <img src="{{ $notification->data['owner_avatar'] }}" width="25px" class="img-circle" alt="User Image">
                        <a href="/user/profile" class="ml-2 font-weight-bold">{{ $notification->data['owner_username'] }}</a> &nbsp; commented on &nbsp;
                        <div><a href="/thread/{{ $notification->data['thread_slug'] }}" class="font-weight-bold ">{{ strip_tags($notification->data['thread_subject']) }}</a></div>
                    </div>
                    <div class="text-muted">{{ \Carbon\carbon::create($notification->data['reply_created_at'])->diffForHumans() }}</div>
                </div>
            </div>
            <div class="card-body">
                {!! $notification->data['reply_body'] !!}
            </div>
        </div>
        @empty
        <p class="text-center my-5 h4 font-weight-bold">No new Notification yet. Please check back later.</p>
        @endforelse

    </div>
</div>
@endsection
