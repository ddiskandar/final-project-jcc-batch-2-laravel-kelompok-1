@extends('layouts.master')

@section('content-header')
<h1 class="font-weight-bold">Leaderboard</h1>
@endsection

@section('main-content')
<div class="row">
    <div class="col">
        @foreach ($users as $user)
        <div class="card ">
            <div class="card-body">
                <div class="d-md-flex justify-content-between ">
                    <div class="d-flex align-items-center">
                        <span class="font-weight-bold h2 mr-4">{{ $loop->iteration }}</span>
                        <img src="{{ $user->profile_photo_url }}" width="40px" height="40px" class="img-circle" alt="User Image">
                        <span><a href="/profile/{{ $user->id }}" class="ml-3 font-weight-bold ">{{ $user->username }}</a></span>
                    </div>
                    <div class="d-flex align-items-center text-muted">
                        <span class="card-link">{{ $user->threads->count() }} threads</span>
                        <span class="card-link">{{ $user->replies->count() }} Replies</span>
                        <span class="card-link">{{ $user->countSolutions() }} Solution</span>
                        <span class="card-link">{{ DB::table('likables')->where('user_id', $user->id)->count() }} Likes</span>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
