<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="{{ route('home') }}" class="navbar-brand">
            <img src="https://jabarcodingcamp.id/assets/img/Logo/LogoJCC-desktop.svg" alt="AdminLTE Logo"
                class="brand-image" style="opacity: .8">
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="/leaderboard" class="nav-link">Leaderboard</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form method="GET" action="/" class="form-inline ml-0 ml-md-3">
                @if (request('category'))
                    <input type="hidden" name="category" value="{{ request('category') }}">
                @endif
                <div class="input-group input-group-sm">
                    <input name="search" class="form-control form-control-navbar" type="search"
                        placeholder="Find something" aria-label="Search" value="{{ request('search') }}">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>


        <!-- Right navbar links -->
        <ul class="order-1 order-md-3  navbar-nav ml-auto navbar-no-expand">
            @guest
                <li class="nav-item">
                    <a class="btn btn-light px-3 mx-2 m-lg-1" href="{{ route('register') }}">
                        Register
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-info px-3 mx-2 m-lg-1" href="{{ route('login') }}">
                        Login
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="/notifications">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">{{ ( auth()->user()->notifications_count > 9 ) ? '9+' : auth()->user()->notifications_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-light px-3 mx-2 m-lg-1" href="/profile/{{ Auth::user()->id }}">
                        <!-- Href harus menuju url profile dengan mengirimkan id user, supaya tampilan di halaman profile nanti sesuai -->
                        <img src="{{ Auth::user()->profile_photo_url }}" width="20px" class="img-circle"
                            alt="User Image">
                        <span class="ml-2">{{ Auth::user()->username }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-info px-3 mx-2 m-lg-1" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endguest
        </ul>
    </div>
</nav>
<!-- /.navbar -->
