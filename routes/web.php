<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'ThreadController@index')->name('home');
Route::get('leaderboard', 'LeaderboardController@show');
Route::resource('/thread', 'ThreadController');

Route::middleware(['auth', 'verified'])->group(function () {
    // HALAMAN STATIS
    // Route::view('show', 'pages.show');
    // Route::view('create', 'pages.create');
    // Route::view('user/profile', 'pages.profile');

    //RESOURCES
    Route::resource('/category', 'CategoryController');
    Route::resource('/profile', 'ProfileController');

    //CRUD
    Route::post('thread/{thread}/replies', 'ThreadReplyController@store');
    Route::post('thread/{thread}/{reply}/likes', 'ReplyLikeController@store');
    Route::delete('thread/{thread}/{reply}', 'ThreadReplyController@destroy');
    Route::post('thread/{thread}/likes', 'ThreadLikeController@store');
    Route::post('thread/{thread}/spam', 'ThreadSpamController@store');
    Route::post('thread/{thread}/not-spam', 'ThreadSpamController@destroy');
    Route::post('thread/{thread}/{replies}/not-spam', 'ReplySpamController@destroy');
    Route::post('thread/{thread}/{replies}/spam', 'ReplySpamController@store');
    Route::get('replies/{reply}/edit', 'ReplyController@edit');
    Route::put('replies/{reply}', 'ReplyController@update');
    Route::post('thread/{thread}/replies/{reply}/solution', 'ThreadReplySolutionController@store');
    Route::get('notifications', 'NotificationsController@index');
    Route::post('notifications', 'NotificationsController@update');
    Route::DELETE('notifications', 'NotificationsController@destroy');
    Route::delete('replies/{reply}/{userid}', 'ReplyController@destroy');
    Route::delete('threads/{thread}/{userid}', 'ThreadController@deleteFromProfile');

});

Auth::routes();
Auth::routes(['verify' => true]);
