# Final Project JCC Batch 2

## Kelas Laravel Kelompok 1

## Anggota Kelompok
- Kartika Azari
- Fillah Zaki Alhaqi 
- Dede Iskandar

## Tema Project
Pilihan 2 : Membuat Forum Tanya Jawab

## ERD
![ERD](https://gitlab.com/ddiskandar/final-project-jcc-batch-2-laravel-kelompok-1/-/raw/master/ERD.png)


## Link Video
Link Demo Aplikasi  : [youtube.com](https://youtube.com)

Link Deploy         : [http://ftanyajawab.herokuapp.com/](http://ftanyajawab.herokuapp.com/)

## Akun Demo
### Admin
email : admin@example.com

password : administrator


### Moderator
email : moderator@example.com

password : moderator


### Member
email : member@example.com

password : password

## Requirements
- [x] User harus terdaftar di web untuk menggunakan layanan Forum
- [x] Satu kategori dapat memiliki banyak pertanyaan.
- [x] User dapat menghapus, mengedit, dan menambah kategori
- [x] User dapat membuat pertanyaan berupa: tulisan, gambar, dan kategorinya.
- [x] User dapat membuat, mengedit, dan menghapus pada pertayaan milik sendiri.
- [x] Seorang User dapat memberi jawaban dari pertayaan User lainnya. Sebuah pertayaan dapat memiliki banyak jawaban dari user yang berbeda.
- [x] User dapat membuat, menghapus dan mengedit jawaban milik sendiri
- [x] Seorang User dapat mengubah profile nya sendiri.
- [x] Pada halaman profile terdapat biodata, email, umur, dan alamat
- [x] pasang minimal 3 Library/Package di luar Laravel : sweet alert, Datatables, TinyMCE.
- [x] Deploy ke Heroku atau share hosting

## Ketentuan
- [x] Membuat ERD
- [x] Migrations
- [x] Model + Eloquent
- [x] Controller
- [x] Laravel Auth + Middleware
- [x] View (Blade)
- [x] CRUD (Create Read Update Delete)
- [x] Eloquent Relationships
- [x] Laravel + Library/Packages

## Fitur Unggulan
- filter untuk mencari thread spesifik melalui kata kunci pencarian, kategori, latest, top views, top likes, top replies
- **Leaderboard** untuk melihat daftar terbaik user dengan jumlah thread, reply, solution dan likes
- user dapat memberikan **like** pada thread dan reply
- user pembuat thread dapat memberikan **Mark as solution** dari reply pada thread miliknya
- user dapat **Mark as Spam** untuk thread atau reply spam
- **notifikasi** untuk melihat pemberitahuan bila ada yang memberikan reply pada thread miliknya
- halaman **profil** untuk melihat stat user

## Desain Breaf
- user ada tiga tipe, yaitu 1 Member, 2 Moderator dan 3 Admin
- Admin mempunyai semua akses yang dimiliki member dan Moderator
- Hanya Admin yang dapat menghapus, mengedit, dan menambah kategori
- hanya pembuat thread/reply yang bisa mengedit thread/reply miliknya
- Hanya Moderator dan Admin yang bisa melihat laporan spam dan  menghapus thread/reply nya
- 3 library yang diterapkan : sweet alert, Datatables, TinyMCE
