<?php

use App\Profile;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $admin = factory(App\User::class)
        ->create([
            'type' => User::ADMIN,
            'email' => 'admin@example.com',
            'username' => 'administrator',
            'password' => bcrypt('administrator'),
        ]);

        factory(App\Profile::class)
        ->create([
            'user_id' => $admin->id,
            'name' => 'Administrator',
            'bio' => 'Super Admin',
        ]);

        // Moderator
        $moderator = factory(App\User::class)
        ->create([
            'type' => User::MODERATOR,
            'email' => 'moderator@example.com',
            'username' => 'moderator',
            'password' => bcrypt('moderator'),
        ]);

        factory(App\Profile::class)
        ->create([
            'user_id' => $moderator->id,
            'name' => 'Moderator',
            'bio' => 'Moderator',
        ]);

        // Moderator
        $member = factory(App\User::class)
        ->create([
            'type' => User::DEFAULT,
            'email' => 'member@example.com',
            'username' => 'member',
            'password' => bcrypt('password'),
        ]);

        factory(App\Profile::class)
        ->create([
            'user_id' => $member->id,
            'name' => 'Member',
            'bio' => 'Member',
        ]);

        for($i = 1; $i <= 50; $i++){
            $member = factory(App\User::class)
            ->create();

            factory(App\Profile::class)
            ->create([
                'user_id' => $member->id,
            ]);
        }

        factory(App\Category::class)->create(['name' => 'General', 'slug' => 'general']);
        factory(App\Category::class)->create(['name' => 'Laravel', 'slug' => 'laravel']);
        factory(App\Category::class)->create(['name' => 'Blade', 'slug' => 'blade']);
        factory(App\Category::class)->create(['name' => 'Vue', 'slug' => 'vue']);
        factory(App\Category::class)->create(['name' => 'Reactjs', 'slug' => 'reactjs']);
        factory(App\Category::class)->create(['name' => 'Flutter', 'slug' => 'flutter']);
        factory(App\Category::class)->create(['name' => 'Nodejs', 'slug' => 'nodejs']);
        factory(App\Category::class)->create(['name' => 'Golang', 'slug' => 'golang']);
        factory(App\Category::class)->create(['name' => 'Livewire', 'slug' => 'livewire']);
        factory(App\Category::class)->create(['name' => 'Intertia', 'slug' => 'intertia']);

        for($i = 1; $i <= 76; $i++){
            factory(App\Thread::class)->create([
                'user_id' => rand(1, 50),
                'category_id' => rand(1, 10),
            ]);
        }

        for($i = 1; $i <= 300; $i++){
            factory(App\Reply::class)->create([
                'user_id' => rand(1, 50),
                'thread_id' => rand(1, 76),
            ]);
        }

    }
}
