<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Thread;
use Faker\Generator as Faker;

$factory->define(Thread::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class),
        'category_id' => factory(App\Category::class),
        'slug' => $faker->slug,
        'subject' => $faker->sentence,
        'body' => $faker->paragraph(3),
        'views_count' => rand(0, 9999),
        'spam_reports' => rand(0, 5),
    ];
});
