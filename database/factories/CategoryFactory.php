<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'name' => $faker->word,
        'description' => $faker->paragraph
    ];
});
