<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class),
        'name' => $faker->name,
        'age' => rand(20,40),
        'address' => $faker->address,
        'bio' => $faker->sentence,
        'last_access' => now(),
    ];
});
