<?php

namespace App\Http\Controllers;

use App\Notifications\ReplyAdded;
use App\Reply;
use App\Thread;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ThreadReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function store(Request $request, Thread $thread)
    {
        $validated = $request->validate([
            'body' => 'required'
        ]);

        $reply = $thread->replies()->create([
            'user_id' => auth()->id(),
            'body' => $validated['body']
        ]);

        $thread->creator->notify( new ReplyAdded($reply));

        Alert::success('Success', 'Reply has been posted');

        return back();
    }

    public function destroy(Thread $thread, $id)
    {
        $reply = Reply::find($id);
        $reply->delete();
        Alert::success('Deleted', 'Reply has been deleted');

        return back();
    }

}
