<?php

namespace App\Http\Controllers;

use App\Reply;
use App\User;
use App\Thread;
use App\Profile;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ReplyController extends Controller
{
    public function edit(Reply $reply)
    {
        return view('reply.edit', [
            'reply' => $reply,
        ]);
    }

    public function update(Reply $reply, Request $request)
    {
        $validated = $request->validate([
            'body' => 'required',
        ]);

        $reply->update([
            'body' =>$validated['body'],
        ]);

        Alert::success('Success', 'Reply has been updated');

        return redirect()->route('thread.show', $reply->thread->slug);
    }

    public function destroy($id, $user_id)
    {
        $reply = Reply::find($id);
        $reply->delete();
        Alert::success('Deleted', 'Reply has been deleted');

        $users = User::findOrFail($user_id);
        $user = Profile::where('user_id', '=', $user_id)->first();
        $threads = Thread::query()->withCount('replies','likes')->where('user_id', '=', $user_id)->get();
        $replies = Reply::query()->withCount('likes')->where('user_id', '=', $user_id)->get();
        
        return redirect()->route('profile.show', $user_id);
    }
}
