<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class NotificationsController extends Controller
{
    public function index()
    {
        return view('notifications.index', [
            'notifications' => auth()->user()->unreadNotifications,
        ]);
    }

    public function update()
    {
        auth()->user()->unreadNotifications()->update(['read_at' => now()]);
        Alert::success('Success', 'All Notifications has been marked as read');

        return back();
    }

    public function destroy()
    {
        auth()->user()->notifications()->delete();

        Alert::success('Success', 'All Notifications has been deleted');

        return back();
    }
}
