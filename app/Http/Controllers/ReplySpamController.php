<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ReplySpamController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function store(Thread $thread, $id)
    {
        $reply = Reply::find($id);

        $reply->update([
            'spam_reports' => $reply->spam_reports + 1
        ]);

        return back();
    }

    public function destroy(Thread $thread, $id)
    {
        $reply = Reply::find($id);

        $reply->update([
            'spam_reports' => 0
        ]);

        Alert::success('Success', 'Spam counter has been reseted!');

        return back();
    }
}
