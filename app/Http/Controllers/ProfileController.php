<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\User;
use App\Thread;
use App\Reply;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Profile::where('user_id', '=', $id)->first();
        $users = User::findOrFail($id);
        $threads = Thread::query()->withCount('replies','likes')->where('user_id', '=', $id)->get();
        $replies = Reply::query()->withCount('likes')->where('user_id', '=', $id)->get();
        return view('profile.index', [
            'user' => $user,
            'threads' => $threads,
            'replies' => $replies,
            'users' => $users
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $users = User::findOrFail($id);
        $user = Profile::where('user_id', '=', $id)->first();
        $user->name = $request->name;
        $user->age = $request->age;
        $user->address = $request->address;
        $user->bio = $request->bio;
        $user->website_url = $request->website_url;
        $user->save();

        Alert::success('Success', 'Profile sudah diperbaharui');

        $threads = Thread::query()->withCount('replies','likes')->where('user_id', '=', $id)->get();
        $replies = Reply::query()->withCount('likes')->where('user_id', '=', $id)->get();
        
        return redirect()->route('profile.show', $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
