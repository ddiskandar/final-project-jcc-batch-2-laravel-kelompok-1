<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;

class LeaderboardController extends Controller
{ 

    public function show()
    {
        $users = User::query()
            ->with('threads', 'replies')
            ->withCount('threads')
            ->orderBy('threads_count', 'desc')
            ->take(10)
            ->get();

        return view('leaderboard.show', [
            'users' => $users
        ]);
    }
}
