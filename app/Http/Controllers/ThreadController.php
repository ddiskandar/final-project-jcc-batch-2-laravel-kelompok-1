<?php

namespace App\Http\Controllers;

use App\Thread;
use App\User;
use App\Profile;
use App\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['thread_count'] = DB::table('threads')->count();

        $threads = Thread::query()
            ->with('category:id,slug,name')
            ->withCount('replies','likes')
            ->filter(
                request(['search', 'author', 'category', 'popular', 'spam'])
            )
            ->latest()
            ->simplePaginate(5);

        return view('thread.index', [
            'data' => $data,
            'threads' => $threads,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( ! Auth::check())
        {
            return redirect()->route('login');
        }
        return view('thread.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'subject' => 'required',
            'body' => 'required',
            'category_id' => 'required',
            'thumbnail' => 'nullable|mimes:png,jpg,jpeg|max:1024',
        ]);

        $thread = Thread::create([
            'user_id' => auth()->id(),
            'subject' => $validated['subject'],
            'body' => $validated['body'],
            'category_id' => $validated['category_id'],
            'slug' => Str::slug($validated['subject'], '-'),
        ]);

        if($request->hasFile('thumbnail'))
        {
            $thumbnail = $request->thumbnail;
            $file_name = time().'-'.$thumbnail->getClientOriginalName();
            $thumbnail->move('images/thumbnail/', $file_name);
            $thread->update([
                'thumbnail' => 'images/thumbnail/' . $file_name,
            ]);
        }

        Alert::success('Success', 'New Discussion has been created');

        return redirect()->route('thread.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        $thread->update([
            'views_count' => $thread->views_count + 1
        ]);

        return view('thread.show', [
            'thread' => $thread
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        return view('thread.edit', [
            'thread' => $thread
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        $validated = $request->validate([
            'subject' => 'required',
            'body' => 'required',
            'category_id' => 'required',
            'thumbnail' => 'nullable|mimes:png,jpg,jpeg|max:1024',
        ]);

        $thread->update([
            'user_id' => auth()->id(),
            'subject' => $validated['subject'],
            'body' => $validated['body'],
            'category_id' => $validated['category_id'],
            'slug' => Str::slug($validated['subject'], '-'),
        ]);

        if($request->hasFile('thumbnail'))
        {
            File::delete($thread->thumbnail);
            $thumbnail = $request->thumbnail;
            $file_name = time().'-'.$thumbnail->getClientOriginalName();
            $thumbnail->move('images/thumbnail/', $file_name);
            $thread->update([
                'thumbnail' => 'images/thumbnail/' . $file_name,
            ]);
        }

        Alert::success('Update', 'Thread has been updated');

        return redirect()->route('thread.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        $thread->delete();
        Alert::success('Deleted', 'Thread has been deleted');

        return redirect()->route('thread.index');
    }

    public function deleteFromProfile($id, $user_id)
    {
        $thread = Thread::find($id);
        $thread->delete();
        Alert::success('Deleted', 'Thread has been deleted');

        $users = User::findOrFail($user_id);
        $user = Profile::where('user_id', '=', $user_id)->first();
        $threads = Thread::query()->withCount('replies','likes')->where('user_id', '=', $user_id)->get();
        $replies = Reply::query()->withCount('likes')->where('user_id', '=', $user_id)->get();

        return redirect()->route('profile.show', $user_id);
    }
}
