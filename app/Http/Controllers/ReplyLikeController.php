<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Thread;
use Illuminate\Http\Request;

class ReplyLikeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    } 
    
    public function store(Thread $thread, Reply $reply)
    {
        if( ! auth()->check())
        {
            return redirect()->route('login');
        }

        $reply->like(auth()->user());

        return back();
    }
}
