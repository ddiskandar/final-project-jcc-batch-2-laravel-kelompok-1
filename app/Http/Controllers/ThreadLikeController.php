<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;

class ThreadLikeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function store(Thread $thread)
    {
        if( ! auth()->check())
        {
            return redirect()->route('login');
        }

        $thread->like(auth()->user());

        return back();
    }
}
