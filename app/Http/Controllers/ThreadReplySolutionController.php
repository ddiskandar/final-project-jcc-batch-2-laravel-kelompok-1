<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Thread;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ThreadReplySolutionController extends Controller
{
    public function store(Thread $thread, Reply $reply)
    {
        if(!$thread->user_id == auth()->id())
        {
            return;
        }

        $thread->update([
            'solution_reply_id' => $reply->id,
        ]);

        Alert::success('Succuess', 'Thread Solution has been updated');

        return back();
    }
}
