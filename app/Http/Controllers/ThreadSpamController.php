<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ThreadSpamController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function store(Thread $thread)
    {
        $thread->update([
            'spam_reports' => $thread->spam_reports + 1
        ]);

        return back();
    }

    public function destroy(Thread $thread)
    {
        $thread->update([
            'spam_reports' => 0
        ]);

        Alert::success('Success', 'Spam counter has been reseted!');

        return back();
    }

}
