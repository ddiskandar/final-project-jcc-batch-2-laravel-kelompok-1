<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'replies';

    protected $fillable = [
        'user_id',
        'thread_id',
        'body',
        'thumbnail',
        'views_count',
        'spam_reports'
    ];

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo('App\Thread', 'thread_id');
    }

    public function like($user= null)
    {
        $user = $user ?: auth()->user();

        return $this->isLikedBy(auth()->user()) ? $this->likes()->detach($user) : $this->likes()->attach($user);
    }

    public function likes()
    {
        return $this->morphToMany('App\User', 'likable')->withTimestamps();
    }

    public function isLikedBy(User $user)
    {
        return $this->likes()->where('user_id', $user->id)->exists();
    }

    public function isUpdated()
    {
        return $this->updated_at->gt($this->created_at);
    }

    public function solutionTo()
    {
        return $this->hasOne(Thread::class, 'solution_reply_id');
    }

    public function scopeIsSolution(Builder $builder): Builder
    {
        return $builder->has('solutionTo');
    }

    public function isThreadSolution()
    {
        return $this->id == $this->thread->solution_reply_id;
    }
}
