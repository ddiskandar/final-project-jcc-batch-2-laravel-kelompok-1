<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Thread extends Model
{
    protected $table = 'threads';

    protected $fillable = [
        'user_id',
        'category_id',
        'slug',
        'subject',
        'body',
        'thumbnail',
        'views_count',
        'spam_reports',
        'solution_reply_id'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function excerpt(int $limit = 265): string
    {
        return Str::limit(strip_tags($this->body), $limit);
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function like($user= null)
    {
        $user = $user ?: auth()->user();

        return $this->isLikedBy(auth()->user()) ? $this->likes()->detach($user) : $this->likes()->attach($user);
    }

    public function likes()
    {
        return $this->morphToMany('App\User', 'likable')->withTimestamps();
    }

    public function isLikedBy(User $user)
    {
        return $this->likes()->where('user_id', $user->id)->exists();
    }

    public function isSolved()
    {
        return null !== $this->solution_reply_id;
    }

    public function isUpdated()
    {
        return $this->updated_at->gt($this->created_at);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search) {
            $query->where(function($query) use ($search) {
                $query->where('subject', 'like', '%' . $search . '%')
                    ->orWhere('body', 'like', '%' . $search . '%');
            });
        });

        $query->when($filters['category'] ?? false, function ($query, $category) {
            $query->whereHas('category', function ($query) use ($category) {
                $query->where('slug', $category);
            });
        });

        $query->when($filters['author'] ?? false, function ($query, $author) {
            $query->where('user_id', $author);
        });

        $query->when(\Request::has('spam') && \Request::query('spam') == 'threads' ?? false, function ($query) {
            $query->orderBy('spam_reports', 'DESC');
        });

        $query->when(\Request::has('popular') && \Request::query('popular') == 'views' ?? false, function ($query) {
            $query->orderBy('views_count', 'DESC');
        });

        $query->when(\Request::has('popular') && \Request::query('popular') == 'likes' ?? false, function($query) {
            $query->orderBy('likes_count', 'DESC');
        });

        $query->when(\Request::has('popular') && \Request::query('popular') == 'replies' ?? false, function($query) {
            $query->orderBy('replies_count', 'DESC');
        });
    }

    public function getThumbnailUrlAttribute()
    {
        return $this->thumbnail
                    ? asset($this->thumbnail)
                    : asset('images/thumbnail-placeholder.svg');
    }
}
